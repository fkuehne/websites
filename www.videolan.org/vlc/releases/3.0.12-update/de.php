            <center><h1 class='bigtitle' style="padding-bottom: 3px;">Problem bei der <em>automatischen Aktualisierung</em> von VLC <b>3.0.12</b> und <b>3.0.13</b></h1>
            <div style="padding-top: 0px; padding-bottom: 10px; color: grey;">Ein Fehler wird die automatische Aktualisierung f&uuml;r Nutzer*innen unter Windows verhindern</div>
            </center>

        <div class="container">

    <center><h2>Dies betrifft nur Nutzer*innen unter Windows</h2></center>

<h3>Kurzfassung:</h3>
<ul>
<li>- Die Versionen 3.0.12 und 3.0.13 k&ouml;nnen <b>nicht</b> automatisch aktualisiert werden; eine aktive Aktion der Nutzer*in ist <b>notwendig</b><li>
<li>- Die Versionen 3.0.11 und davor k&ouml;nnen automatisch auf 3.0.14 aktualisiert werden</li>
</ul>
<br/>

<h3>Beschreibung:</h3>
Die Bekanntmachung betrifft die Nutzer*innen von VLC 3.0.13 und VLC 3.0.12.<br/>
Auf Grund eines Fehlers im automatischen Aktualisierungsprozess werden Updates heruntergeladen, auf Integrit&auml;t gepr&uuml;ft, jedoch nicht installiert. Das ist schlecht und wir m&ouml;chten uns daf&uuml;r entschuldigen.<br/><br/>

<h3>Anleitung:</h3>
Um auf Version 3.0.14 zu aktualisieren, m&uuml;ssen Sie auf <a href="https://www.videolan.org/vlc">https://www.videolan.org/vlc</a> gehen, um VLC manuell herunterzuladen und zu installieren.<br/>
Sie k&ouml;nnen hierzu Details in englischer Sprache auf <a href="https://docs.videolan.me/vlc-user/3.0/en/gettingstarted/setup/windows.html">dieser Seite</a> finden.<br/><br/>
Falls Sie bereits den Updater ausgef&uuml;hrt und das Installationsprogramm heruntergeladen haben, k&ouml;nnen Sie es manuell ausf&uuml;hren, in dem Sie den Explorer &ouml;ffnen (Windows-Taste + E oder Klick auf das Explorer-Icon) und <em>%TEMP%</em> als Ort eingeben.<br/>
Dort werden Sie das Installationsprogramm sehen. Es wird «vlc-3.0.14-win32.exe» oder «vlc-3.0.14-win64.exe» hei&szlig;en, je nach dem ob Sie eine 32bit oder 64bit Fassung von Windows verwenden.<br/>
<br/>
<?php image("screenshots/3.0.12-update.jpg" , "3.0.12 update screen", "center-block img-responsive"); ?>
<br/>
<br/>

<h3>Post mortem Erkl&auml;rung:</h3>
Am 10. Mai 2021 ver&ouml;ffentlichte die VideoLAN Organisation VLC 3.0.13 und aktivierte die Verteilung &uuml;ber den automatischen Aktualisierungsprozess.<br/>
Dies w&uuml;rde typischerweise ein gradliniger Vorgang sein, ein Dialog informiert die Nutzer*in &uuml;ber eine verf&uuml;gbare Aktualisierung, Sie klicken Herunterladen und Installieren und das w&auml;re das Ende der Angelegenheit.
Indessen und ungl&uuml;cklicherweise sind f&uuml;r diese bestimmte Aktualisierung zus&auml;tzliche, erm&uuml;dende manuelle Schritte erforderlich.<br/>
Das Problem wurde in Version 3.0.12 in den Verkehr gebracht, offenbarte sich jedoch nicht bis zur Ausrollung von 3.0.13.<br/>
W&auml;hrend das Problem f&uuml;r 3.0.14 gel&ouml;st wurde, kann die Korrektur nicht f&uuml;r Nutzer*innen angewendet werden, welche bereits 3.0.12 installiert haben.<br/>

<br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/83d8e7efaa4f7dc23b07c47c59431e1f6df57da5">
Die &Auml;nderung, die den Fehler einbrachte</a><br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/d456994213b98933664bd6aee2e8f09d5dea5628">Die &Auml;nderung, die den Fehler f&uuml;r zuk&uuml;nftige Ver&ouml;ffentlichungen l&ouml;ste</a><br/><br/>


