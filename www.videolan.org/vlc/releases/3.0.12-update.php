<?php

$documents['en'] = array(
    "title" => "VLC 3.0.12 and 3.0.13 auto updater issues",
    "lang" => "ENGLISH",
);

$documents['de'] = array(
    "title" => "Probleme bei der automatischen Aktualisierung von VLC 3.0.12 und 3.0.13",
    "lang" => "DEUTSCH",
);

$documents['fr'] = array(
    "title" => "Problème dans le système de mises à jour automatique</em> de VLC 3.0.12 et 3.0.13",
    "lang" => "FRAN&Ccedil;AIS",
);

$documents['hi'] = array(
    "title" => "&#2357;&#2368;&#2319;&#2354;&#2360;&#2368; 3.0.12 &#2324;&#2352; 3.0.13 &#2360;&#2381;&#2357;&#2330;&#2366;&#2354;&#2367;&#2340; &#2309;&#2346;&#2337;&#2375;&#2335;&#2352; &#2360;&#2370;&#2330;&#2344;&#2366;",
    "lang" => "&#2361;&#2367;&#2306;&#2342;&#2368;",
);

$documents['it'] = array(
    "title" => "Problema nell'aggiornamento automatico in VLC 3.0.12 e 3.0.13",
    "lang" => "ITALIANO",
);

$documents['jp'] = array(
    "title" => "VLC3.0.12および3.0.13自動アップデーターの問題",
    "lang" => "日本語",
);

$documents['pt'] = array(
    "title" => "Problema com o atualizador automático no VLC 3.0.12 e 3.0.13",
    "lang" => "PORTUGUÊS",
);

$documents['ru'] = array(
    "title" => "&#1055;&#1088;&#1086;&#1073;&#1083;&#1077;&#1084;&#1072; &#1089; &#1072;&#1074;&#1090;&#1086;&#1084;&#1072;&#1090;&#1080;&#1095;&#1077;&#1089;&#1082;&#1080;&#1084; &#1086;&#1073;&#1085;&#1086;&#1074;&#1083;&#1077;&#1085;&#1080;&#1077;&#1084; VLC 3.0.12 &#1080; 3.0.13",
    "lang" => "&#1056;&#1059;&#1057;&#1057;&#1050;&#1048;&#1049;",
);

function display_lang(&$documents, $language)
{
    if( isset($documents[$language]) )
        return $language;
    else
        return "en";
}

function make_elements(&$documents, $language)
{
    foreach($documents as $key => &$value)
    {
        if( !isset($documents[$language]) || $language == $key )
            $elements[] = "<b>".$value["lang"]."</b>";
        else if($key == "en")
            $elements[] = "<a href=\"3.0.12-update.html\">".$value["lang"]."</a>";
        else
            $elements[] = "<a href=\"3.0.12-update.".$key.".html\">".$value["lang"]."</a>";
    }
    return implode($elements, " | ");
}

$additional_inline_css = array("
 body {
      -webkit-font-smoothing: antialiased;
      color: #232525;
    }

    .subtext { position: relative; top: -20px; left:250px; width: 400px; text-align: center; }
    .subtext2 { position: relative; top: -20px; width: 100%; text-align: center; }
    .features ul li:before {
        padding-left: 18px;
        width: 10px;
        height: 10px;
    }
    .features ul li {
        margin-bottom: 8px;
    }
    .audienceCallout {
        font-weight: bold;
        margin-top: 20px;
    }
    .embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto; }
    .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
    section.no-check ul li {
        list-style-type: initial;
    }
    section.no-check ul li:before {
        content: none;
    }
    .slide-container {
        z-index: 1;
    }
    .text-container {
        z-index: 0;
    }
");

   $body_color = "red";
   $language = display_lang($documents, $language);
   $nobanner = true;
   $new_design = true;
   $additional_css= array("/js/css/slimbox2.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/os-specific.php");
   $title = $documents[$language]["title"];
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
   require($_SERVER["DOCUMENT_ROOT"]."/include/package.php");

   $macosxversion = "3.0.14";
   $win32version = "3.0.14";
?>
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="row text-center">
                <p><?php echo make_elements($documents, $language); ?></p>
            </div>
        </div>
    </div>


    <div class="container">
    <?php
        require("3.0.12-update/".display_lang($documents, $language).".php");
    ?>
    </div>

<!-- Downloads -->
        <div class="container">

        <h1>Download Updated VLC</h1>
        <div class="row">

        <div class="col-md-12"><div>
        <p>
           <a id='downloadButton' href='<?php echo getDownloadLink("vlc", $win32version, "win32/vlc-$win32version-win32.exe"); ?>' >
               <img style='position: absolute; top: -10px; left: -10px;' src='/images/downloadVLC.png' alt='Download VLC icon' />
               <span class='downloadText'><?php echo _("VLC for Windows"); ?></span>
               <span style='font-size: 12px; color: white;'>
               Version <span><?php echo $win32version ?></span></span>
           </a>
        </p>
        </div></div>

        </div>

        <div class="row">

        <div class="col-md-6">
        <h1>Related links</h1>
        <ul>
            <li><a href="https://code.videolan.org/videolan/vlc/-/raw/3.0.x/NEWS">Changelog</a></li>
        </ul>

        </div>

        <div class="col-md-6">
        <h1>Contact</h1>
        <p>For any questions related to this release, please <a href="/contact.html">contact us</a>.</p>
        </div>

        </div>
<?php footer('$Id: index.php 7173 2012-02-11 00:58:09Z jb $'); ?>
