# Icelandic translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Sveinn í Felli <sv1@fellsnet.is>, 2012-2019
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2019-07-02 04:11+0200\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>, 2019\n"
"Language-Team: Icelandic (http://www.transifex.com/yaron/vlc-trans/language/"
"is/)\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "það er þróunarverkefni og"

#: include/header.php:289
msgid "non-profit organization"
msgstr "samtök án hagnaðarmarkmiða"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Samstarfsaðilar"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Teymi og skipulag"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Ráðgjafarþjónustur og samstarfsaðilar"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Atburðir"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Lagalegt"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Fjölmiðlar"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Hafa samband"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Sækja"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Eiginleikar"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Sérsníða"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Ná í aukadót"

#: include/menus.php:51
msgid "Projects"
msgstr "Verkefni"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Öll verkefni"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Taka þátt"

#: include/menus.php:77
msgid "Getting started"
msgstr "Að komast í gang"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Styðja verkefnið"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Tilkynna um villu"

#: include/menus.php:83
msgid "Support"
msgstr "Aðstoð"

#: include/footer.php:33
msgid "Skins"
msgstr "Skinn"

#: include/footer.php:34
msgid "Extensions"
msgstr "Viðbætur"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Skjámyndir"

#: include/footer.php:61
msgid "Community"
msgstr "Samfélag"

#: include/footer.php:64
msgid "Forums"
msgstr "Spjallsvæði"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Póstlistar"

#: include/footer.php:66
msgid "FAQ"
msgstr "Algengar spurningar - FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Gefa peninga"

#: include/footer.php:68
msgid "Donate time"
msgstr "Gefa tíma"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Verkefni og skipulag"

#: include/footer.php:76
msgid "Team"
msgstr "Teymi"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Speglar"

#: include/footer.php:83
msgid "Security center"
msgstr "Öryggismál"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Vera með"

#: include/footer.php:85
msgid "News"
msgstr "Fréttir"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Ná í VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Önnur stýrikerfi"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "þegar sótt"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC er frjáls og opinn margmiðlunarspilari og bakvinnslukerfi fyrir mörg "
"stýrikerfi sem spilar flestar gerðir margmiðlunarskráa, DVD-mynddiska, CD-"
"hljóðdiska, VCD, auk ýmissa tegunda streymis."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC er frjáls og opinn margmiðlunarspilari og bakvinnslukerfi fyrir mörg "
"stýrikerfi sem spilar flestar gerðir margmiðlunarskráa auk ýmissa tegunda "
"streymis. "

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC: Opinber vefur verkefnisins - Frjálsar margmiðlunarlausnir fyrir öll "
"stýrikerfi!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Önnur verkefni á vegum VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Fyrir alla"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC er öflugur margmiðlunarspilari sem ræður við flestar þær gerðir "
"kóðunarlykla (codecs) og vídeósniða sem finnast."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator er hugbúnaður fyrir ólínulega vinnslu myndskeiða."

#: index.php:62
msgid "For Professionals"
msgstr "Fyrir atvinnumenn"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr "DVBlast er einfalt og öflugt forrit fyrir MPEG-2/TS demux og streymi."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat er verkfærasett sem hannað er til að meðhöndla skilvirkt á "
"einfaldan hátt multicast strauma og TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 er frjálst forrit til að kóða vídeóstrauma yfir á H.264/MPEG-4 AVC snið."

#: index.php:104
msgid "For Developers"
msgstr "Fyrir forritara"

#: index.php:140
msgid "View All Projects"
msgstr "Skoða öll verkefni"

#: index.php:144
msgid "Help us out!"
msgstr "Réttu okkur hjálparhönd!"

#: index.php:148
msgid "donate"
msgstr "styðja"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN eru samtök án hagnaðarmarkmiða."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Öllum kostnaði er mætt með gjafafé sem við fáum frá notendum. Ef VLC-"
"hugbúnaður kemur þér að góðu gagni, endilega gefðu eitthvað til að styðja "
"okkur."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Vita meira"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN er opinn og frjáls hugbúnaður."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Það þýðir að langi þig til og hafir kunnáttu til að bæta eitthvað í "
"verkefnunum okkar, þá eru framlög þín meira en velkomin."

#: index.php:187
msgid "Spread the Word"
msgstr "Láta orð út ganga"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Okkur finnst að VideoLAN sé með besta vídeómeðhöndlunarhugbúnaðinn sem "
"fáanlegur er fyrir besta verðið: ókeypis. Ef þú ert sammála, endilega láttu "
"fólk í kringum þig vita hvað þér finnst um hugbúnaðinn okkar."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Fréttir og uppfærslur"

#: index.php:218
msgid "More News"
msgstr "Fleiri fréttir"

#: index.php:222
msgid "Development Blogs"
msgstr "Blogg varðandi forritaþróunina"

#: index.php:251
msgid "Social media"
msgstr "Samfélagsmiðlar"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"Opinbert niðurhal VLC margmiðlunarspilarans, besta opna og frjálsa spilarans"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Ná í VLC fyrir"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Einfaldur, hraðvirkur og öflugur"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Spilar allt"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "skrár, diska, vefmyndavélar, inntakstæki og streymi."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""
"Spilar flesta kóðunarlykla (codecs) án þess að þurfa auka-kóðunarlyklapakka"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Keyrir á öllum stýrikerfum"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Algerlega frjálst og ókeypis"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""
"engin njósnaforrit, engar auglýsingar og ekki fylgst með notkun þess á neinn "
"hátt."

#: vlc/index.php:47
msgid "learn more"
msgstr "vita meira"

#: vlc/index.php:66
msgid "Add"
msgstr "Bæta við"

#: vlc/index.php:66
msgid "skins"
msgstr "skinn"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Búa til skinn með"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC skinnaritlinum"

#: vlc/index.php:72
msgid "Install"
msgstr "Setja upp"

#: vlc/index.php:72
msgid "extensions"
msgstr "viðbætur"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Skoða allar skjámyndir"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Opinber niðurhöl VLC margmiðlunarspilarans"

#: vlc/index.php:146
msgid "Sources"
msgstr "Grunnkóði"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Þú getur líka komist beint í"

#: vlc/index.php:148
msgid "source code"
msgstr "grunnkóðann"

#~ msgid "A project and a"
#~ msgstr "Þróunarverkefni og"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "byggð á sjálfboðaliðum sem hanna og kynna frjálsar og opnar "
#~ "margmiðlunarlausnir."

#~ msgid "why?"
#~ msgstr "af hverju?"

#~ msgid "Home"
#~ msgstr "Heim"

#~ msgid "Support center"
#~ msgstr "Hjálparmiðstöð"

#~ msgid "Dev' Zone"
#~ msgstr "Hakkarasvæði"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Einfaldur, hraðvirkur og öflugur margmiðlunarspilari."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Spilar allt: skrár, diska, vefmyndavélar, inntakstæki og strauma."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr ""
#~ "Spilar flesta kóðunarlykla (codecs) án þess að þurfa auka-"
#~ "kóðunarlyklapakka:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Keyrir á öllum kerfum:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Algerlega frjálst/ókeypis, engin njósnaforrit, engar auglýsingar og ekki "
#~ "fylgst með notkun þess á neinn hátt."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Getur umbreytt og streymt flestum miðlum."

#~ msgid "Discover all features"
#~ msgstr "Skoða alla eiginleika"

#~ msgid "DONATE"
#~ msgstr "STYÐJA"

#~ msgid "Other Systems and Versions"
#~ msgstr "Önnur stýrikerfi og útgáfur"

#~ msgid "Other OS"
#~ msgstr "Önnur stýrikerfi"
