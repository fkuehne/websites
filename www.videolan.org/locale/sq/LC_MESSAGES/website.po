# Albanian translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Ardit Dani <ardit.dani@gmail.com>, 2014,2019
# Emmanuel Malaj <emmanuelmalaj@outlook.com>, 2015
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2019-04-22 22:22+0200\n"
"Last-Translator: Ardit Dani <ardit.dani@gmail.com>, 2019\n"
"Language-Team: Albanian (http://www.transifex.com/yaron/vlc-trans/language/"
"sq/)\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "një projekt dhe një"

#: include/header.php:289
msgid "non-profit organization"
msgstr "organizatë jo-fitimprurëse"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Partnerë"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Ekip &amp; Organizatë"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Shërbime Konsulence &amp; Partnerë"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Ngjarje"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Ligjshmëria"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Qendra shtypit"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Na kontaktoni"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Shkarko"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Veçori"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Personalizuar"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Merrni Sende"

#: include/menus.php:51
msgid "Projects"
msgstr "Projekte"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Të gjithë Projektet"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Kontribuoj"

#: include/menus.php:77
msgid "Getting started"
msgstr "Fillimi"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Dhuro"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Raporto nje bug"

#: include/menus.php:83
msgid "Support"
msgstr "Mbështetje"

#: include/footer.php:33
msgid "Skins"
msgstr "Fytyrat"

#: include/footer.php:34
msgid "Extensions"
msgstr "Zgjerime"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Pamjet"

#: include/footer.php:61
msgid "Community"
msgstr "Komuniteti"

#: include/footer.php:64
msgid "Forums"
msgstr "Forume"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Liste poste"

#: include/footer.php:66
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Dhuroni lekë"

#: include/footer.php:68
msgid "Donate time"
msgstr "Dhuroni kohë"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Projekti dhe Organizata"

#: include/footer.php:76
msgid "Team"
msgstr "Skuadra"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Pasqyra"

#: include/footer.php:83
msgid "Security center"
msgstr "Qendra sigurisë"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Përfshihuni"

#: include/footer.php:85
msgid "News"
msgstr "Lajme"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Shkarko VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Sistemet e tjera"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "shkarkuar deri më tani"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC eshte nje multimedia player falas, open source, disa platformesh qe "
"suporton shumicen e dokumenteve multimedia si dhe DVD, CD Audio, VCD dhe "
"protokolle te ndryshme transmetimi."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC është një lexues ndër-platformë multimedia i lirë dhe me burim të hapur "
"dhe strukturë që luan shumicën e dosjeve multimedia dhe protokolle të "
"ndryshme të transmetimit."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC: Faqja zyrtare - Zgjidhje multimediash falas per te gjitha sistemet e "
"operimit!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Projekte të tjera nga VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Për të Gjithë"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC eshte nje media player i fuqishem duke luajtur shumicen e kodeve te "
"medias dhe formateve te videove qe ekzistojne"

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Krijuesi Filmave është një program jo-linear për montimin dhe "
"krijimin e videove."

#: index.php:62
msgid "For Professionals"
msgstr "Për Profesionalë"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast është një program i thjeshtë dhe i fuqishëm MPEG-2/TS demux dhe "
"transmetues."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat eshte nje paketa me mjete te dizenjuara për të manipuluar thjeshte "
"dhe ne menyre eficente transmetimet multicast dhe TS. "

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 është një aplikacion falas për kodimin e transmetimeve te videove në "
"formatin H.264/MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "Për Zhvillues"

#: index.php:140
msgid "View All Projects"
msgstr "ShikoTë gjithë Projektet"

#: index.php:144
msgid "Help us out!"
msgstr "Na ndihmoni!"

#: index.php:148
msgid "donate"
msgstr "dhuro"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN është një organizatë jo-fitimprurëse."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Të gjitha shpenzimet tona janë plotësuar nga donacionet që marrim nga "
"përdoruesit tanë. Nëse ju pëlqeni përdorimin një produkti të VideoLAN, ju "
"lutemi dhuroni për të na mbështetur."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Mëso më Shume"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN është program burim-hapur."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Kjo do të thotë se në qoftë se ju keni aftësi dhe dëshironi për të "
"përmirësuar një nga produktet tona, kontributet tuaja janë të mirëpritura"

#: index.php:187
msgid "Spread the Word"
msgstr "Shpërndani Fjalën"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Ne mendojmë se VideoLAN ka programin me te mirë për video në dispozicion me "
"çmimim më të mirë: falas. Nëse jeni dakord ju lutem ndihmoni në përhapjen e "
"fjalës në lidhje me programin tonë."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Lajme &amp; Azhurnime"

#: index.php:218
msgid "More News"
msgstr "Më shumë Lajme"

#: index.php:222
msgid "Development Blogs"
msgstr "Blogje te zhvillimit"

#: index.php:251
msgid "Social media"
msgstr "Media sociale"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr "Shkarkimi zyrtar i Lexuesi VLC media, lexuesi më i mirë i Open Source"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Merr VLC për"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "I thjeshtë, i shpejtë dhe i fuqishëm"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Luan gjithçka"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Skedarë, Disqe, Kamera, Pajisje dhe Transmetime."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Luan shumicën e kodekseve pa paketat e kodifikimit të nevojshëm"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Punon në të gjitha platformat"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Plotësisht Falas"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "pa spyware, pa reklama dhe pa ndjekje të përdoruesve."

#: vlc/index.php:47
msgid "learn more"
msgstr "Mëso më shumë"

#: vlc/index.php:66
msgid "Add"
msgstr "Shto"

#: vlc/index.php:66
msgid "skins"
msgstr "pamjet"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Krijo pamjet me"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "Modifikues pamjesh VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "Instalo"

#: vlc/index.php:72
msgid "extensions"
msgstr "zgjerime"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Shiko te gjitha pamjet"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Shkarkime zyrtare të VLC media player"

#: vlc/index.php:146
msgid "Sources"
msgstr "Burimet"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Ju gjithashtu mund të merrni direkt"

#: vlc/index.php:148
msgid "source code"
msgstr "kodin burim"

#~ msgid "A project and a"
#~ msgstr "Një projekt dhe një"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "përbërë nga vullnetarë, duke zhvilluar dhe promovuar falas, zgjidhje "
#~ "multimediash, open-source."

#~ msgid "why?"
#~ msgstr "pse?"

#~ msgid "Home"
#~ msgstr "Shtëpi"

#~ msgid "Support center"
#~ msgstr "Qendra mbështetjes"

#~ msgid "Dev' Zone"
#~ msgstr "Zonë Zhvillimi"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Media player i thjeshtë, i shpejtë dhe i fuqishëm."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Luan gjithçka: Skedarë, Disqe, Kamera, Pajisje dhe Transmetim."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Luan shumicen e kodimeve pa nevojen e paketave te kodeve:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Punon në të gjitha platformat:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Krejtesisht Falas, pa spyware, pa reklama dhe pa gjurmim perdoruesi."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Mund te beje konvertim dhe transmetim mediash."

#~ msgid "Discover all features"
#~ msgstr "Zbulo të gjithë karakteristikat"
