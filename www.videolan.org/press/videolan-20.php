<?php
   $title = "Press Release - VideoLAN is 20 years old!";
   $lang = "en";
   $menu = array( "vlc" );
   $new_design = true;
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>
<style>
 body {
      color: #232525;
    }
p { font-size:14px; line-height: 26px; }
.subtext { position: relative; top: -20px; left:300px; width: 300px; text-align: center; }
.productDescription li { margin-left: 20px; font-size: 13px; }

h2 { margin-top: 20px; margin-bottom: 12px; }
h3 { color: #999; font-size: 16px; }
em { font-style: italic; }

</style>

<div class="container" style="max-width: 1000px !important;">
<h1>VideoLAN is 20 years old today!</h1>
<br />

<div class="longtext productDescription">
<div class="date"><b>Paris, Feb 1<sup>st</sup> 2021</b></div>

<h2>VideoLAN celebrates its 20 years anniversary</h2>

<p>The <b>VideoLAN</b> project and the <a href="/videolan/">VideoLAN</a> non-profit organization are happy to celebrate today the 20<sup>th</sup> anniversary of the open-sourcing of the project.</p>


<p><b>VideoLAN</b> originally started as a project from the <a href="https://viarezo.fr/">Via Centrale Réseaux</a>
 student association, after the successful Network 2000 project.<br /> But the true release of the project to the world was on 1st of February 2001, the <a href="https://www.centralesupelec.fr/">École Centrale Paris</a> director, Mr. Gourisse, allowed the open-sourcing of the whole VideoLAN project under the <a href="https://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU GPL</a>.</p>

<p>This open sourcing concerned all the software developed by the VideoLAN project, including <em>VideoLAN Client</em>, <em>VideoLAN Server</em>, <em>VideoLAN Bridge</em>, <em>VideoLAN Channel Switcher</em>, but also libraries to decode DVDs, like <em>libdca</em>, <em>liba52</em> or <em>libmpeg2</em>.</p>
<p>At that time, this was a risky decision for the <a href="https://www.centralesupelec.fr/">École Centrale Paris</a>, and the VideoLAN project is very grateful.</em>

<div class="row">
<a href="//images.videolan.org/images/VideoLAN_GPL.jpeg"><?php image("VideoLAN_GPL.jpeg" , "VideoLAN decision to GPL", "center-block img-responsive col-md-6"); ?></a>
</div>

<h3>Evolution</h3>
<p>Since then, the project evolved to become a French non-profit organization, and continued developing numerous solutions around the free software multimedia world.</p>

<p>Today, <b>VLC media player</b> is used regularly by hundreds of millions of users, and has been downloaded more than 3.5 billion times over the years. VLC is today available on Windows, macOS, Linux, Android <em>(including TV and Auto versions)</em>, iOS <em>(and AppleTV)</em>, OS/2 and BSD.</p>

<p>Over the years, around <b>1000 volunteers</b> worked to make VLC a reality.</p>
<p>We're working on the next versions of VLC, to bring better video and audio quality, better subtitle options, improved UI and other improvements asked by our community.</p>

<p>In addition to the popular <a href="/vlc/">VLC media player</a> <em>(the new name of VideoLAN Client)</em>, VideoLAN also developed <b>x264</b>, the most popular
software encoder in the world, streaming software like DVBlast, DVD and Blu-Ray stack libraries or, recently the <a href="https://code.videolan.org/videolan/dav1d">dav1d</a> AV1 decoder.</p>

<?php image("events/vdd18.jpg" , "VDD 2018", "center-block img-responsive"); ?>

<h2>Celebrations</h2>
<p>Several events are planned online and in real-life, and they will be announced through the 2021 year.<br />
Because of the current pandemic, the real-life celebrations for this event will be delayed,
for the second half of the year.</p>

<p>As a first step, we're currently contacting most of the major contributors to VideoLAN and VLC,
including developers, event organizers, forum helpers, documentation writers and other parts of the community to
send a very special and unique package.</p>
<p>If you are part of those, please check that we have your address!</p>

<p>Stay tuned for more news!</p>

<div class="row"><div class="col-md-6 col-lg-2 col-xl-1 col-centered mx-auto">
<iframe src="https://www.youtube-nocookie.com/embed/jWx1P93nS0c" width="560" height="315" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div></div>


<h2>About VideoLAN</h2>

<p><a href="/">VideoLAN</a> is a non-profit organization, run by volunteers, that supports the development of free and open source multimedia solutions.<br />
Its most famous distributed software, the <a href="/vlc">VLC media player</a>, is completely free and also one of the most used programs in the world. The application is free of advertisements, spyware, bloatware and other misfeatures that could be deceptive to its users.</p>

<p>Visit <a href="http://www.videolan.org/">http://www.videolan.org</a>.</p>

<h3>Press Contact</h3>
<p>press@videolan.org</p>

<?php image("events/vdd08.jpg" , "First VDD", "center-block img-responsive"); ?>

</div>
</div>
<?php footer('$Id:$'); ?>

