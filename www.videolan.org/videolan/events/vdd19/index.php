<?php
   $title = "Video Dev Days 2019, November 9-10, 2019";
   $body_color = "orange";

   $new_design = true;
   $show_vdd_banner = false;
   $lang = "en";
   $menu = array( "videolan", "events" );

   $rev = 1;

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/videolan/events/vdd19/style.css?$rev");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>
<script>
  countdownManager = {
    targetTime: new Date(),
    element: {
        day: null,
        hour: null,
        min: null,
        sec: null
    },
    interval: null,
    init: function(targetTime) {
        this.targetTime = targetTime || new Date();
        this.element.day  = $('#countdown-day');
        this.element.hour = $('#countdown-hour');
        this.element.min  = $('#countdown-min');
        this.element.sec  = $('#countdown-sec');

        this.tick();
        this.interval = setInterval('countdownManager.tick();', 1000);
    },

    tick: function() {
        var timeNow = new Date();
        if (timeNow > this.targetTime) {
            timeNow = this.targetTime;
            $('#countdown').hide();
            clearInterval(this.interval);
            return;
        }

        var diff = this.dateDiff(timeNow, this.targetTime);

        this.element.day.text(diff.day);
        this.element.hour.text(diff.hour);
        this.element.min.text(diff.min);
        this.element.sec.text(diff.sec);
    },

    dateDiff: function(date1, date2) {
        var diff = {};
        var tmp = date2 - date1;

        tmp = Math.floor(tmp / 1000);
        diff.sec = tmp % 60;
        tmp = Math.floor((tmp-diff.sec) / 60);
        diff.min = tmp % 60;
        tmp = Math.floor((tmp-diff.min) / 60);
        diff.hour = tmp % 24;
        tmp = Math.floor((tmp-diff.hour) / 24);
        diff.day = tmp;
        return diff;
    }
};

$(function($){
    countdownManager.init(new Date('2019-11-09'));
});

</script>
  <div class="sponsor-box-2 d-xs-none d-md-block">
    <h4>Sponsors</h4>
    <a href="https://www.iij.ad.jp/en/" target="_blank">
        <?php image( 'events/vdd19/sponsors/IIJ.jpg' , 'Internet Initiative Japan', 'sponsors-logo'); ?>
    </a>
    <a href="https://www.stream.co.jp/english/" target="_blank">
        <?php image( 'events/vdd19/sponsors/jstream.jpg' , 'J-Stream', 'sponsors-logo'); ?>
    </a>
    <a href="https://mux.com/" target="_blank">
        <?php image( 'events/vdd19/sponsors/mux.png' , 'Mux', 'sponsors-logo'); ?>
    </a>
    <a href="https://abema.tv/" target="_blank">
        <?php image( 'events/vdd19/sponsors/abematv.png' , 'AbemaTV', 'sponsors-logo'); ?>
    </a>
    <a href="https://mozilla.org/" target="_blank">
        <?php image( 'events/vdd19/sponsors/mozilla.png' , 'Mozilla', 'sponsors-logo'); ?>
    </a>
    <a href="https://videolabs.io/" target="_blank">
        <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo'); ?>
    </a>
    <a href="https://youtube.com/" target="_blank">
        <?php image( 'events/vdd17/sponsors/youtube_logo.svg' , 'YouTube', 'sponsors-logo'); ?>
    </a>
    <a href="#" target="_blank">
        <?php image( 'events/vdd19/sponsors/tvt.png' , 'Tokyo Video Tech', 'sponsors-logo'); ?>
    </a>
    <a href="https://www.4cast.co.jp/" target="_blank">
        <?php image( 'events/vdd19/sponsors/4cast.png' , 'Forecast Communications Inc.', 'sponsors-logo'); ?>
    </a>
  </div>
  <header class="header-bg">
      <div class="container">
        <div class="row col-lg-10 col-lg-offset-1">
          <div class="col-sm-10 col-md-6">
            <img class="img-fluid center-block" style="max-width:100%; height:auto;" src="//images.videolan.org/images/events/vdd19/vdd2019cone.png" alt="vdd logo">
          </div>
          <div class="col-sm-12 col-md-6 text-center align-middle">
            <h1 id="videodevdays">Video Dev Days 2019</h1>
            <h3>The Open Multimedia Conference that frees the cone in you!</h3>
            <h4>9 - 10 November, 2019, Tokyo</h4>
            <div class="row d-xs-none d-md-block">
              <div id="countdown">
                <div class="countdown-box">
                  <span id="countdown-day" >--</span>
                  <div class="countdown-unit">Days</div>
                </div>
                <div class="countdown-box">
                  <span id="countdown-hour">--</span>
                  <div class="countdown-unit">Hours</div>
                </div>
                <div class="countdown-box">
                  <span id="countdown-min" >--</span>
                  <div class="countdown-unit">Minutes</div>
                </div>
                <div class="countdown-box">
                  <span id="countdown-sec" >--</span>
                  <div class="countdown-unit">Seconds</div>
                </div>
              </div>
            </div>
            <div class="row text-center">
              <a id="regbtn" href="https://forms.gle/dUGSE3eG3gyh5SQ6A" class="btn btn-border vbtn-link">Register</a>
            </div>
          </div>
        </div>

      </div>
<!-- 
       <div class="container">
           <div id="sponsors">
           <h5>Sponsors</h5>
           <p>We are looking for sponsors</p>
           </div>
       </div>
-->
   </header>
<section id="overview">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">
          Video Dev Days 2019
          <span class="spacer-inline">About</span>
        </h2>
        <p>The <a href="/videolan/">VideoLAN non-profit organisation</a> is happy to
        invite you to the multimedia open-source event of the fall!</p>
        <p>For its <b>eleventh edition</b>, people from the VideoLAN and open source multimedia communities will meet in <strong>Tokyo</strong>
        to discuss and work on the future of the open-source multimedia community</p>

        <p>This is a <strong>technical</strong> conference, focused on low-level multimedia, like codecs and their implementations like <a href="https://www.videolan.org/developers/x264.html">x264</a> or <a href="https://code.videolan.org/videolan/dav1d">dav1d</a>, frameworks like FFmpeg or Gstreamer or playback libraries like libVLC.</p>
        <div class="row">
          <div class="col-md-6">
            <div class="text-box when-box">
              <h4 class="text-box-title">When?</h4>
              <p class="text-box-content">9 - 10 November 2019</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box where-box">
              <h4 class="text-box-title">Where?</h4>
              <p class="text-box-content">Tokyo</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <section id="where" class="bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">Where?</h2>
        <p>The venue is in Paris!</p>
      </div>
    </div>
  </div>
</section> -->

<section id="schedule">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <h2 class="uppercase">Schedule</h2>
        <hr class="spacer-2">
      </div>
      <div class="col-lg-10 col-lg-offset-1">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#friday" aria-controls="friday" role="tab" data-toggle="tab">Friday 8</a></li>
        <li role="presentation" class="active"><a href="#saturday" aria-controls="saturday" role="tab" data-toggle="tab">Saturday 9</a></li>
        <li role="presentation"><a href="#sunday" aria-controls="sunday" role="tab" data-toggle="tab">Sunday 10</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="friday">
            <div class="event">
              <h4 class="event-time">09:30 - 18:30</h4>
              <div class="event-description">
                <h3>Community Bonding Day: Surprise!</h3>
                <p>Pokecone: <b>Codec Wars !</b><br/>
		The VideoLAN organization will pay for whatever costs associated with the event.<br/>
                Discover your team members in a fun game around Tokyo.<br/>
                Please meet in the sponsored hotel lobby (<a href="https://wiki.videolan.org/VDD19/#Accommodation_.2F_Hotel">Villa Fontaine Nihombashi Hakozaki</a>) at 9:30am.
              </div>
            </div>
            <div class="event">
              <h4 class="event-time">19:30</h4>
              <div class="event-description">
                <h3>Evening drinks</h3>
                <p>On <strong>Friday at 19h30</strong>, people are welcome to come and
                share a few good drinks, with all attendees. <!--, at the <a href="https://goo.gl/maps/tTTAPwjzHe62" target="_blank">King George pub</a>. --></p>
                <p>Alcoholic and non-alcoholic drinks will be available!</b>
              </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade active in" id="saturday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                08:30 - 09:00
              </h4>
              <div class="event-description">
                <h3>Registration</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:00 - 09:10
              </h4>
              <p class="event-author">
                <span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN
              </p>
              <div class="event-description">
                <h3>Welcome words</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:10 - 09:20
              </h4>
              <p class="event-author">
                Marie Gunji, Internet Initiative Japan
              </p>
              <div class="event-description">
                <h3>Internet Initiative Japan</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:20 - 09:50
              </h4>
              <p class="event-author">
                Ronald S. Bultje, Two Orioles
              </p>
              <div class="event-description">
                <h3>dav1d</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:50 - 10:20
              </h4>
              <p class="event-author">
                Phil Cluff, Mux
              </p>
              <div class="event-description">
                <h3>Sash and low latency adaptive streaming</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:20 - 10:50
              </h4>
              <p class="event-author">
                Pradeep Ramachandran, MulticoreWare Inc.
              </p>
              <div class="event-description">
                <h3>x265 update</h3>
              </div>
            </div>


            <div class="event event-breakfast">
              <h4 class="event-time">
                10:50 - 11:05
              </h4>
              <div class="event-description">
                <h3>Break</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:05 - 11:35
              </h4>
              <p class="event-author">
                Thomas Daede, Mozilla 
              </p>
              <div class="event-description">
                <h3>rav1e update</h3>
              </div>
            </div>


            <div class="event event-talk">
              <h4 class="event-time">
                11:35 - 12:05
              </h4>
              <p class="event-author">
                <span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN
              </p>
              <div class="event-description">
                <h3>VLC 4.0</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                12:05 - 12:20
              </h4>
              <div class="event-description">
                <h3>Tokyo Video Tech</h3>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                12:30 - 13:30
              </h4>
              <div class="event-description">
                <h3>Lunch Break</h3>
                <p>Bentō sponsored by <a href="https://www.stream.co.jp/english/" target="_blank">J-Stream</a></p>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                13:30 - 14:00
              </h4>
              <div class="event-description">
                <h3>Group Photo</h3>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Meetups (<a href="https://wiki.videolan.org/VDD19/#Planning">See wiki</a>)</h3>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                19:30 - 21:30
              </h4>
              <div class="event-description">
                <h3>Community Dinner</h3>
                <p>Location: Warayakiya, Ginza<br/>You need to be on time.</p>
              </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="sunday">

            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 12:00
              </h4>
              <div class="event-description">
                <h3>Lightning talks (<a href="https://wiki.videolan.org/VDD19/#Planning">see wiki</a>)</h3>
                <ul>
                </ul>
              </div>
            </div>

            <div class="event event-bg event-lunch">
              <h4 class="event-time">
                12:00 - 14:00
              </h4>
              <div class="event-description">
                <h3>Lunch</h3>
                <p>Bentō sponsored by <a href="https://abema.tv/" target="_blank">AbemaTV</a></p>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Unconferences (<a href="https://wiki.videolan.org/VDD19/#Planning">see wiki</a>)</h3>
              </div>
            </div>

            <div class="event">
              <div class="event-inner">
                <div class="event-description">
                    <p>The actual content of the unconference track is being decided on Saturday evening. For the live schedule, check the <a href="https://wiki.videolan.org/VDD19#Unconference_schedule">designated page on the wiki</a>.</p>
                </div>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                19:30 - 21:30
              </h4>
              <div class="event-description">
                <h3>Unofficial Dinner</h3>
                <p>Karaoke at J-House amusement bar</p>
              </div>
            </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>

<section id="who-come">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-box" id="who-come-box">
        <h2 class="uppercase">Who can come?
        </h2>
        <p><strong>Anyone</strong> who cares about open source multimedia technologies and development. Remember that it targets a technical crowd!</p>
        <p>If you are representing a <b>company</b> caring about open-source multimedia software, we would be <b>very interested</b> if you could co-sponsor the event.</p>
      </div>

    </div>
  </div>
</section>

<section id="register-section">
  <div class="container">
      <h2 class="uppercase">Register</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="text-box register-box">
            <h3 class="text-box-title">
              COST AND SPONSORSHIP
            </h3>
            <h3>FREE</h3>
            <p class="text-box-content">
              The cost for attendance is free.

              Like previous years, active developers can get a full sponsorship covering travel costs. We will also provide accomodation.
            </p>
            <div class="ticket">
              <a href="https://forms.gle/dUGSE3eG3gyh5SQ6A">HERE!</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="text-box register-box">
            <h3 class="text-box-title">Accommodation</h3>
            <p>If you got sponsering confirmation your hotel will be:<br/>
               Hotel Villa Fontaine Tokyo - Nihombashi Hakozaki<br/>
               20-10 Hakozaki-cho Nihombashi Chuo-ku Tokyo, 103-0015</p>
          </div>
        </div>
      </div>
  </div>
</section>
<section>
  <div class="container">
    <h2 class="text-center uppercase">Sponsors</h2>
    <hr class="spacer-2">

    <section class="text-center">
     <div class="row col-xs-10 col-xs-offset-1">
      <div class="col col-md-6 col-sm-12">
        <a href="https://www.iij.ad.jp/en/" target="_blank">
         <?php image( 'events/vdd19/sponsors/IIJ.jpg' , 'Internet Initiative Japan', 'sponsors-logo-2'); ?>
        </a>
      </div>
      <div class="col col-md-6 col-sm-12">
        <a href="https://www.stream.co.jp/english/" target="_blank">
         <?php image( 'events/vdd19/sponsors/jstream.jpg' , 'J-Stream', 'sponsors-logo-2'); ?>
        </a>
      </div>

      <div class="clearfix"></div>

      <div class="col col-md-6 col-sm-12">
        <a href="https://mux.com/" target="_blank">
          <?php image( 'events/vdd19/sponsors/mux.png' , 'Mux', 'sponsors-logo-2'); ?>
        </a>
      </div>
      <div class="col col-md-6 col-sm-12">
        <a href="https://abema.tv/" target="_blank">
          <?php image( 'events/vdd19/sponsors/abematv.png' , 'AbemaTV', 'sponsors-logo-2'); ?>
        </a>
      </div>

      <div class="clearfix"></div>

      <div class="col col-md-6 col-sm-12">
        <a href="https://mozilla.org/" target="_blank">
          <?php image( 'events/vdd19/sponsors/mozilla.png' , 'Mozilla', 'sponsors-logo-2'); ?>
        </a>
      </div>
      <div class="col col-md-6 col-sm-12">
        <a href="https://videolabs.io/" target="_blank">
          <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo-2'); ?>
        </a>
      </div>

      <div class="clearfix"></div>

      <div class="col col-md-6 col-sm-12">
          <?php image( 'events/vdd19/sponsors/tvt.png' , 'Tokyo Video Tech', 'sponsors-logo-2'); ?>
      </div>

      <div class="col col-md-6 col-sm-12">
        <a href="https://youtube.com/" target="_blank">
          <?php image( 'events/vdd17/sponsors/youtube_logo.svg' , 'YouTube', 'sponsors-logo-2'); ?>
        </a>
      </div>

      <div class="clearfix"></div>

      <div class="col col-md-6 col-sm-12">
        <a href="https://www.4cast.co.jp/" target="_blank">
          <?php image( 'events/vdd19/sponsors/4cast.png' , 'Forecast Communications Inc.', 'sponsors-logo-2'); ?>
        </a>
      </div>


     </div>
    </section>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Locations</h2>
    <hr class="spacer-2">
    <div class="row">
      <div class="col-md-6">
        <div class="text-box conference-box">
          <div class="text-box-title">
            Conference
          </div>
          <div class="text-box-content">
            Internet Initiative Japan Inc.
Iidabashi Grand Bloom, 2-10-2 Fujimi, Chiyoda-ku, Tokyo 102-0071, JAPAN
          </div>
        </div>
      </div>
      <!--<div class="col-md-6">
        <div class="text-box dinner-box">
          <div class="text-box-title">
              Saturday Dinner
          </div>
          <div class="text-box-content">
          </div>
        </div>
      </div> -->
    </div>

  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="uppercase">Code of Conduct </h2>
        <p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Contact </h2>
        <p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Denis Charmet, Konstantin Pavlov and Hugo Beauz&eacute;e-Luyssen. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>
      </div>
    </div>
  </div>
</section>

<script>
  $('#videodevdays').html("Vide<span style=\"color:#ff0000;font-weight:bold;\">&#9679;</span> Dev Days 2019");
</script>

<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
